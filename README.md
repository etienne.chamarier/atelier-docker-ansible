# atelier-docker-ansible

## Getting started

To simplify use of the playbook, we recommend installing the necessary modules in a virtual environment.

Inside the project directory:
```bash
python3 -m pip3 install virtualenv
python3 -m venv .venv
source .venv/bin/activate
```

Install the requirements (ansible, ...):

```bash
python3 -m pip install -r requirements.txt
```

Customize the inventory.yml file with your managed node-s.

And launch the playbook:

```bash
ansible-playbook -i inventory.yml [all] playbook.yml
```

## To-do

- [ ] Step 1: create an ansible control node
- [ ] Step 2: create an inventory that contains a remote managed node (hint, it can be a remote lxc container under debian 12)
- [x] Step 3: create an ansible role that will install Docker on the remote managed node
- [x] Step 4: create an ansible role that will install docker-compose
- [x] Step 5: prepare a docker-compose.yml file that runs an nginx image (used the official image)
- [ ] Step 6: create a role that will copy the previously designed docker-compose file and ensure that it is launched on the managed node
- [x] Step 7: create the playbook that allows the play of these ansible roles
- [ ] Step 8: push your project to gitlab or github and make the repository accessible to your trainer
- [x] Step 9: Fill in the "Getting Started" part with some information about this Ansible mini-project.

## Test

Make sure that the nginx http server is working properly by connecting directly to the managed node and executing the following command: 

```bash
curl http://localhost
```
